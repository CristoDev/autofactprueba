<?php

namespace App\Http\Controllers;

use App\Questionary;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $request->user()->authorizeRoles(['user', 'admin']);
        Log::info('User Information');
        Log::info($request->user());
        $quiz = $request->user()->getQuestionaryThisMonth();
        if ($request->user()->hasAnyRole('admin')) {
            return view('admin', ['quiz' => $quiz ?? null]);
        }
        if ($request->user()->hasAnyRole('user')) {
            return view('home', ['quiz' => $quiz ?? null]);
        }
        return view('home');
    }

    /**
     * Save new questionaries response.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function saveQuiz(Request $request)
    {
        $questionary = null;
        $message = null;
        try {
            $request->user()->authorizeRoles(['user', 'admin']);
            Log::info($request);
            $quiz = $request->user()->getQuestionaryThisMonth();
            if (!$quiz) {
                $questionary = new Questionary();

                $questionary->add_report = $request->get('add_report');
                $questionary->is_correct = $request->get('is_correct');
                $questionary->is_fast = $request->get('is_fast');
                $questionary->user_id = $request->user()->id;
                $questionary->save();
                $message = "Cuestionario agregado correctamente.";
            } else {
                $questionary = $quiz;
                $message = "Ya tienes un cuestionario respondido este mes.";
            }
        } catch (\Exception $e) {
            Log::critical("code=[{$e->getCode()}] file=[{$e->getFile()}] line=[{$e->getLine()}] message=[{$e->getMessage()}]");
            $message = "No fue posible realizar el guardado del cuestionario, por favor contacte al administrador.";
        }
        if ($request->user()->hasAnyRole('admin')) {
            $questionaries = Questionary::all();
            return view('admin', ['message' => $message, 'quiz' => $questionary ?? null, 'questionaries' => $questionaries ?? null]);
        }
        if ($request->user()->hasAnyRole('user')) {
            return view('home', ['message' => $message, 'quiz' => $questionary ?? null]);
        }
        return view('home');
    }

    /**
     * Get all questionaries response.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function getQuiz(Request $request)
    {
        $questionaries = null;
        $message = null;
        try {
            $request->user()->authorizeRoles(['admin']);
            $questionaries = Questionary::all();
        } catch (\Exception $e) {
            Log::critical("code=[{$e->getCode()}] file=[{$e->getFile()}] line=[{$e->getLine()}] message=[{$e->getMessage()}]");
            $message = "No fue posible realizar el guardado del cuestionario, por favor contacte al administrador.";
        }
        if ($request->user()->hasAnyRole('admin')) {
            return ['questionaries' => $questionaries ?? null, 'message' => $message];
        }
    }
}

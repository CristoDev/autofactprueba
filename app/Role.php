<?php

namespace App;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{

    /**
     * Get users by rol
     *
     * @return void
     */
    public function roles()
    {
        return $this->belongsToMany(User::class)->withTimestamps();
    }
}

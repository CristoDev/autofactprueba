<?php

namespace App;
use App\Role;
use App\Questionary;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Log;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get user Rol
     *
     * @return void
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class)->withTimestamps();
    }
    /**
     * authorizeRoles
     *
     * @param  mixed $roles
     * @return void
     */
    public function authorizeRoles($roles)
    {
        abort_unless($this->hasAnyRole($roles), 401);
        return true;
    }
    /**
     * hasAnyRole
     *
     * @param  mixed $roles
     * @return void
     */
    public function hasAnyRole($roles)
    {
        if (is_array($roles)) {
            foreach ($roles as $role) {
                if ($this->hasRole($role)) {
                    return true;
                }
            }
        } else {
            if ($this->hasRole($roles)) {
                 return true;
            }
        }
        return false;
    }

    /**
     * hasRole
     *
     * @param  mixed $role
     * @return void
     */
    public function hasRole($role)
    {
        if ($this->roles()->where('name', $role)->first()) {
            return true;
        }
        return false;
    }
    /**
     * Get quiz of the month
     *
     * @return void
     */
    public function getQuestionaryThisMonth(){
        $quiz = Questionary::where('user_id',$this->id)->whereMonth('created_at', '=', date('m'))->first();
        Log::info('$quiz');
        Log::info($quiz);
        return $quiz??false;
    }

}

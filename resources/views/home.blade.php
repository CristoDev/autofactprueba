@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 col-xs-12">
                @if (!empty($message))
                    <div class="alert alert-primary" role="alert">
                        {{ $message }}
                    </div>
                @endif
            </div>

            <div class="col-md-8 col-xs-12">
                <div class="card">
                    <div class="card-header">{{ __('Dashboard Usuario') }}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <form method="POST" action="{{ route('save.quiz') }}">
                            @csrf
                            <div class="form-group row">
                                <label for="add_report"
                                    class="col-md-4 col-form-label text-md-right">{{ __('¿Qué te gustaría que agregaramos al informe?') }}</label>

                                <div class="col-md-6">
                                    <textarea id="add_report" class="form-control @error('add_report') is-invalid @enderror"
                                        name="add_report" @if (!empty($quiz)) {{ 'disabled' }} @endif autocomplete="add_report"
                                        autofocus rows="3"
                                        required>@if (!empty($quiz)) {{ __($quiz->add_report) }} @endif</textarea>
                                    @error('add_report')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="is_correct"
                                    class="col-md-4 col-form-label text-md-right">{{ __('¿La información es correcta?') }}</label>

                                <div class="col-md-6">

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="is_correct" id="is_correct_yes"
                                            value="yes" @if (!empty($quiz)) {{ 'disabled' }}        @if ($quiz->is_correct=='yes'){{ 'checked' }} @endif @else {{ 'checked' }} @endif >
                                        <label class="form-check-label" for="is_correct_yes">
                                            {{ __('SI') }}
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="is_correct" id="is_correct_no"
                                            value="no" @if (!empty($quiz)) {{ 'disabled' }}        @if ($quiz->is_correct=='no'){{ 'checked' }} @endif @endif>
                                        <label class="form-check-label" for="is_correct_no">
                                            {{ __('NO') }}
                                        </label>
                                    </div>
                                    <div class="form-check disabled">
                                        <input class="form-check-input" type="radio" name="is_correct" id="is_correct_so_so"
                                            value="so so" @if (!empty($quiz)) {{ 'disabled' }}        @if ($quiz->is_correct=='so so'){{ 'checked' }} @endif @endif>
                                        <label class="form-check-label" for="is_correct_so_so">
                                            {{ __('Más o Menos') }}
                                        </label>
                                    </div>
                                    @error('is_correct')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="is_fast"
                                    class="col-md-4 col-form-label text-md-right">{{ __('¿Del 1 al 5, es rápido el sitio?') }}</label>

                                <div class="col-md-6">
                                    <input type="range" name="is_fast" @if (!empty($quiz)) {{ 'disabled' }} @endif
                                        class="form-range  form-control @error('is_correct') is-invalid @enderror" min="1"
                                        max="5" step="1" id="is_fast" list="list_range" value="
                                                                      @if (!empty($quiz))
                                    {{ __($quiz->is_fast) }} @endif">
                                    <datalist id="list_range">
                                        <option value="1" label="{{ __('1') }}"></option>
                                        <option value="2">{{ __('2') }}</option>
                                        <option value="3">{{ __('3') }}</option>
                                        <option value="4">{{ __('4') }}</option>
                                        <option value="5">{{ __('5') }}</option>
                                    </datalist>
                                    @error('is_fast')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary" @if (!empty($quiz)) {{ 'disabled' }} @endif>
                                        {{ __('Enviar Respuestas') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

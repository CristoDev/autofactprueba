<?php

use Illuminate\Database\Seeder;
use App\Role;
class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = new Role();
        $role->name = 'admin';
        $role->description = 'Administrator - Puede ver además una vista de los envíos realizados';
        $role->save();
        $role = new Role();
        $role->name = 'user';
        $role->description = 'Usuario - Puede solo responder(solo una vez al mes) y ver su última respuesta';
        $role->save();
    }
}

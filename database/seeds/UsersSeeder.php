<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\User;
use Illuminate\Support\Facades\Hash;
class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::create([
            'name' => "Administrador Autofact",
            'email' => "admin@autofact.cl",
            'password' => Hash::make("admin1234.,"),
        ]);
        $admin->roles()->attach(Role::where('name', 'admin')->first());

        $user = User::create([
            'name' => "Usuario Autofact",
            'email' => "user@autofact.cl",
            'password' => Hash::make("user1234.,"),
        ]);
        $user->roles()->attach(Role::where('name', 'user')->first());
    }
}
